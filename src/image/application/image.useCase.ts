import { ImageRepository, ImageEntity, ImageValue } from '../domain'

export class ImageUseCases {
  constructor(private readonly _imageRepository: ImageRepository) {}

  public async getByUser(doc_user: string) {
    const images = await this._imageRepository.getByUser(doc_user)
    return images
  }
  public async save(imageIn: ImageEntity) {
    const image = new ImageValue(imageIn)
    const imageDb = await this._imageRepository.save(image)
    return imageDb
  }
  public async update(id: string, imageIn: ImageEntity) {
    const result = await this._imageRepository.update(id, imageIn)
    return result
  }
  public async delete(doc_user: string) {
    const result = await this._imageRepository.delete(doc_user)
    return result
  }
}
