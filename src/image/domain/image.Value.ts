import { v4 as generate_uuid } from 'uuid'
import { ImageEntity } from './image.Entity'
export class ImageValue implements ImageEntity {
  uuid: string
  doc_user: string
  url: string
  fileName: string

  constructor({
    doc_user,
    url,
    fileName,
    uuid = generate_uuid(),
  }: ImageEntity) {
    this.uuid = uuid
    this.doc_user = doc_user
    this.url = url
    this.fileName = fileName
  }
}
