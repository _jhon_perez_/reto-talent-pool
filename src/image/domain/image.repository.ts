import { ImageEntity } from './image.Entity'
export interface ImageRepository {
  getByUser(doc_user: string): Promise<string[] | false>
  save(image: ImageEntity): Promise<any>
  update(id: string, image: ImageEntity): Promise<any>
  delete(id: string): Promise<any>
}
