import { ImageUseCases } from '../../application/image.useCase'
import { Request, Response } from 'express'
import {
  deleteFiles,
  uploadFile,
} from '../../../server/infrastructure/common/helpers/aws.S3'
import { deleteImageTem } from '../../../server/infrastructure/common/helpers/deletedImgTemp'
export class ImageController {
  constructor(private readonly _imageUseCases: ImageUseCases) {}

  public create = async (req: Request, res: Response, next: Function) => {
    const { doc_user = '' } = req.body
    const imageFile: any = req.files?.image
    try {
      const imageIn = await uploadFile(imageFile)
      const imageSaved = await this._imageUseCases.save({
        doc_user,
        ...imageIn,
      })
      await deleteImageTem()

      return res.status(201).json({
        statusCode: 200,
        message: 'Image saved successfully',
        data: imageSaved,
      })
    } catch (error) {
      next(error)
    }
  }

  public getAllByUser = async (req: Request, res: Response, next: Function) => {
    const { doc_user } = req.params
    try {
      const arrImages = await this._imageUseCases.getByUser(doc_user)

      if (!arrImages)
        return res.status(400).json({
          statusCode: 400,
          message: `No images found by userId ${doc_user}`,
          data: arrImages,
        })

      return res.status(200).json({
        statusCode: 200,
        message: 'Images found successfully',
        data: arrImages.map((image: any) => image.url),
      })
    } catch (error) {
      next(error)
    }
  }

  public update = async (req: Request, res: Response, next: Function) => {
    const { id } = req.params
    const imageIn = req.body

    try {
      const result = await this._imageUseCases.update(id, imageIn)
      if (!result)
        return res.status(400).json({
          statusCode: 400,
          message: `No image found by id ${id}`,
          data: result,
        })

      return res.status(200).json({
        statusCode: 200,
        message: 'Image updated successfully',
        data: result,
      })
    } catch (error) {
      next(error)
    }
  }

  public delete = async (req: Request, res: Response, next: Function) => {
    const { doc_user } = req.params
    try {
      const arrImages = await this._imageUseCases.getByUser(doc_user)

      if (!arrImages)
        return res.status(400).json({
          statusCode: 400,
          message: `No image found by id ${doc_user}`,
          data: false,
        })

      const result = await this._imageUseCases.delete(doc_user)
      await deleteFiles(result.map((item: any) => item.fileName))

      return res.status(200).json({
        statusCode: 200,
        message: 'Image deleted successfully',
        data: true,
      })
    } catch (error) {
      next(error)
    }
  }
}
