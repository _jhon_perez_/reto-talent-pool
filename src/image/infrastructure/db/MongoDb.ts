import mongoose from 'mongoose'
import { configApp } from '../../../server/infrastructure/common/configApp'

const { URI_MONGO } = configApp.database.mongo

export class ConnectionMongo {
  async connect() {
    try {
      await mongoose.connect(URI_MONGO)
      console.log('MongoDb is Connectioned !!')
      return true
    } catch (error) {
      console.log('Error Initializing MongoDb!!')
      throw error
    }
  }

  async close() {
    await mongoose.connection.close()
    console.log('Closed is connection MongoDb !!')
  }
}
