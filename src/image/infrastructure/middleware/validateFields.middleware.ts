import { Request, Response, NextFunction } from 'express'
import { validationResult, check } from 'express-validator'
export const validateFieldsImage = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const error = validationResult(req)
  if (!error.isEmpty())
    return res.status(400).json({
      statusCode: 400,
      message: 'FAILED fields!',
      data: { ...error },
    })

  next()
}
export const checkFieldGET = [
  check('doc_user').notEmpty().withMessage('Is required'),
  validateFieldsImage,
]
export const checkFieldPOST = [
  check('doc_user').notEmpty().withMessage('Is required'),
  validateFieldsImage,
]

export const checkFieldPUT = [
  check('id').isUUID().notEmpty().withMessage('Is required'),
  check('doc_user').optional(),
  validateFieldsImage,
]

export const checkFieldDELETE = [
  check('doc_user').notEmpty().withMessage('Is required'),
  validateFieldsImage,
]

export const validateEmptyImage = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (!req.files?.image)
    return res.status(400).json({
      statusCode: 400,
      message: 'No image uploaded',
      data: [],
    })
  next()
}
