import { Schema, model } from 'mongoose'
import { ImageEntity } from '../../domain/image.Entity'

const ImageSchema = new Schema<ImageEntity>(
  {
    uuid: {
      type: 'string',
      required: true,
    },
    doc_user: {
      type: 'string',
      required: true,
    },
    fileName: {
      type: 'string',
      required: true,
    },
    url: {
      type: 'string',
      required: true,
    },
  },
  {
    timestamps: true,
  }
)

export const ImageModel = model('Image', ImageSchema)
