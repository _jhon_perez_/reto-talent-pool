import { ImageEntity, ImageRepository } from '../../domain'
import { ImageModel } from '../models/image.model'

export class MongoRepository implements ImageRepository {
  private readonly _modelImage = ImageModel

  async getByUser(doc_user: string): Promise<any> {
    try {
      const arrImages = await this._modelImage.find({ doc_user })
      if (arrImages.length === 0) return false
      return arrImages
    } catch (error) {
      throw error
    }
  }

  async getById(imageUid: string): Promise<any> {
    try {
      const imageDb = await this._modelImage.findOne({ uuid: imageUid })
      if (!imageDb) return false
      return imageDb
    } catch (error) {
      throw error
    }
  }

  async save(image: ImageEntity): Promise<any> {
    try {
      const imageIn = new this._modelImage(image)
      const imageSaved = await imageIn.save()
      return imageSaved.url
    } catch (error) {
      throw error
    }
  }

  async update(id: string, image: ImageEntity): Promise<any> {
    try {
      const updatedImage = await this.getByUser(id)
      if (!updatedImage) return false
      const result = await updatedImage.update(image)
      return true
    } catch (error) {
      throw error
    }
  }

  async delete(doc_user: string): Promise<any> {
    try {
      const deletedImage = await this.getByUser(doc_user)
      if (!deletedImage) return false
      await this._modelImage.deleteMany({ doc_user })
      return true
    } catch (error) {
      throw error
    }
  }
}
