import { Router } from 'express'
// import multer from 'multer'

import { MongoRepository } from '../repository/mongo.Repository'
import { ImageModel } from '../models/image.model'
import { ImageUseCases } from '../../application/image.useCase'
import { ImageController } from '../controller/image.controller'
import {
  checkFieldPOST,
  checkFieldPUT,
  checkFieldDELETE,
  checkFieldGET,
  validateEmptyImage,
} from '../middleware/validateFields.middleware'
const routesImage = Router()

// Init Repository
const imageRepository = new MongoRepository()

// Init UseCases
export const imageUseCases = new ImageUseCases(imageRepository)

// Init Controller
const imageController = new ImageController(imageUseCases)

routesImage
  .get('/image/:doc_user', checkFieldGET, imageController.getAllByUser)
  .post('/image', checkFieldPOST, validateEmptyImage, imageController.create)
  .put('/image/:id', checkFieldPUT, imageController.update)
  .delete('/image/:doc_user', checkFieldDELETE, imageController.delete)

export default routesImage
