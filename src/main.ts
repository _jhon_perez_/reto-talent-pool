import 'dotenv/config'
import { Server, configApp } from './server/infrastructure/'
import { ConnectionMysql } from './person/infrastructure/db/TypeOrm'
import { ConnectionMongo } from './image/infrastructure/db/MongoDb'

const { server } = configApp
const connectionMongo = new ConnectionMongo()
const connectionMysql = new ConnectionMysql()

const app = new Server(server.PORT, connectionMongo, connectionMysql)

async function main(app: Server) {
  try {
    await app.start()
  } catch (error) {
    console.log(error)
    process.exit(1)
  }
}

main(app)
