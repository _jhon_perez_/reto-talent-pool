import { PersonRepository, PersonEntity, PersonValue } from '../domain'

export class PersonUseCase {
  constructor(private readonly _peronRepository: PersonRepository) {}

  public async getPersons() {
    const person = await this._peronRepository.getAll()
    return person
  }

  public async getPersonById(id: string) {
    const person = await this._peronRepository.getById(id)
    return person
  }

  public async createPerson(personIn: PersonEntity) {
    const person = new PersonValue(personIn)
    const personDB = await this._peronRepository.create(person)
    return personDB
  }

  public async update(id: string, person: PersonEntity) {
    const updatedPerson = await this._peronRepository.update(id, person)
    return updatedPerson
  }

  public async delete(id: string) {
    const deletedPerson = await this._peronRepository.delete(id)
    return deletedPerson
  }
}
