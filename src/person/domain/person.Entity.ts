export interface PersonEntity {
  uuid?: string
  names: string
  lastnames: string
  type_doc: string
  num_doc: string
  age: number
}
