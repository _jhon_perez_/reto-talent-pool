import { PersonEntity } from './person.Entity'

export interface PersonRepository {
  getAll(): Promise<PersonEntity[] | null>
  getById(id: string): Promise<PersonEntity | null>
  create(person: PersonEntity): Promise<PersonEntity | null>
  update(id: string, person: PersonEntity): Promise<any>
  delete(id: string): Promise<boolean>
}
