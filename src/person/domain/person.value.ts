import { v4 as generateUuid } from 'uuid'
import { PersonEntity } from './person.Entity'

export class PersonValue implements PersonEntity {
  uuid: string
  names: string
  lastnames: string
  type_doc: string
  num_doc: string
  age: number

  constructor({
    names,
    lastnames,
    type_doc,
    num_doc,
    age,
    uuid = generateUuid(),
  }: PersonEntity) {
    this.uuid = uuid
    this.names = names
    this.lastnames = lastnames
    this.type_doc = type_doc
    this.num_doc = num_doc
    this.age = age
  }
}
