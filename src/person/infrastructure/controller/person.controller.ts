import { Response, Request } from 'express'
import { PersonUseCase } from '../../application/person.useCase'
import { ImageUseCases } from '../../../image/application/image.useCase'

export class PersonController {
  constructor(
    private readonly _personUseCases: PersonUseCase,
    private readonly _imageUseCases: ImageUseCases
  ) {}

  public create = async (req: Request, res: Response, next: Function) => {
    const { names, lastnames, type_doc, num_doc, age } = req.body

    try {
      const person = await this._personUseCases.createPerson({
        names,
        lastnames,
        type_doc,
        num_doc,
        age,
      })
      if (!person) {
        return res.status(400).json({
          statusCode: 400,
          message: 'Person not saved!',
          data: [],
        })
      }
      return res
        .status(200)
        .json({ statusCode: 200, message: 'Person saved!', data: person })
    } catch (error: any) {
      next(error)
    }
  }

  public getAllPersons = async (
    req: Request,
    res: Response,
    next: Function
  ) => {
    try {
      const persons = await this._personUseCases.getPersons()

      if (!persons)
        return res.status(400).json({
          statusCode: 400,
          message: 'No persons found!',
          data: [],
        })
      const arrPerons = persons.map(async (item) => {
        const imagesbyUser = await this._imageUseCases.getByUser(item.num_doc)
        return {
          ...item,
          images: imagesbyUser ? imagesbyUser.map((item: any) => item.url) : [],
        }
      })
      const arrPersons = await Promise.all(arrPerons)
      return res.status(200).json({
        statusCode: 200,
        message: 'Persons found successfully!',
        data: arrPersons,
      })
    } catch (error) {
      next(error)
    }
  }

  public getPersonById = async (
    req: Request,
    res: Response,
    next: Function
  ) => {
    const { num_doc } = req.params
    try {
      const personDb = await this._personUseCases.getPersonById(num_doc)

      if (!personDb)
        return res
          .status(400)
          .json({ statusCode: 400, message: 'Person no found !', data: [] })

      const imagesbyUser = await this._imageUseCases.getByUser(num_doc)

      return res.status(200).json({
        satus: 200,
        message: 'Person found sucefully!',
        data: {
          ...personDb,
          images: imagesbyUser
            ? imagesbyUser.map((image: any) => image.url)
            : [],
        },
      })
    } catch (error) {
      next(error)
    }
  }

  public updatePerson = async (req: Request, res: Response, next: Function) => {
    const personIn = req.body
    const { num_doc } = req.params

    try {
      const personDb = await this._personUseCases.getPersonById(num_doc)
      if (!personDb) {
        return res.status(400).json({
          statusCode: 400,
          message: 'person no found !',
          data: [],
        })
      }
      await this._personUseCases.update(num_doc, personIn)
      return res.status(200).json({
        statusCode: 200,
        message: 'Person Updated Successfully!',
        data: { num_doc: num_doc, ...personIn },
      })
    } catch (error) {
      next(error)
    }
  }

  public deletePerson = async (req: Request, res: Response, next: Function) => {
    const { num_doc } = req.params

    try {
      const personDb = await this._personUseCases.delete(num_doc)
      if (!personDb) {
        return res.status(400).json({
          statusCode: 400,
          message: 'person no found !',
          data: [],
        })
      }
      await this._imageUseCases.delete(num_doc)
      return res.status(200).json({
        statusCode: 200,
        message: 'Person Delete Successfully!',
        data: [],
      })
    } catch (error) {
      next(error)
    }
  }
}
