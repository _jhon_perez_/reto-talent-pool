import 'reflect-metadata'
import { DataSource } from 'typeorm'
import { ModelPerson } from '../models/person.model'
import { configApp } from '../../../server/infrastructure/common/configApp'

const { MYSQL_HOST, MYSQL_PORT, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE } =
  configApp.database.mysql

export const AppDataSource = new DataSource({
  type: 'mysql',
  host: MYSQL_HOST,
  port: MYSQL_PORT,
  username: MYSQL_USER,
  password: MYSQL_PASSWORD,
  database: MYSQL_DATABASE,
  synchronize: true,
  logging: false,
  entities: [ModelPerson],
  subscribers: [],
  migrations: [],
  poolSize: 1,
})

export class ConnectionMysql {
  async connect() {
    try {
      await AppDataSource.initialize()
      console.log('Mysql is Connectioned!!')
      return true
    } catch (error) {
      console.log('Error Initializing Mysql!!')
      throw error
    }
  }

  async disconnect() {
    if (AppDataSource.isInitialized) {
      try {
        await AppDataSource.destroy()
        console.log('Closed is connection Mysql !!')
      } catch (error) {
        console.log(error)
      }
    }
  }
}
