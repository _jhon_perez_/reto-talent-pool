import { Request, Response, NextFunction } from 'express'
import { validationResult, check } from 'express-validator'

export const validateFieldsPerson = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const error = validationResult(req)
  if (!error.isEmpty())
    return res.status(400).json({
      statusCode: 400,
      message: 'FAILED fields!',
      data: { ...error },
    })

  next()
}
export const checkFieldGET = [
  check('num_doc').notEmpty().withMessage('Is required'),
  validateFieldsPerson,
]
export const checkFieldPOST = [
  check('names').notEmpty().withMessage('Is Requerid!').isString(),
  check('lastnames').notEmpty().withMessage('Is Requerid!').isString(),
  check('type_doc').notEmpty().withMessage('Is Requerid!').isString(),
  check('num_doc').notEmpty().withMessage('Is Requerid!').isString(),
  check('age').notEmpty().withMessage('Is Requerid!'),
  validateFieldsPerson,
]

export const checkFieldPUT = [
  check('num_doc').notEmpty().withMessage('Is required'),
  check('names').optional().isString(),
  check('lastnames').optional().isString(),
  check('type_doc').optional().isString(),
  check('num_doc').optional().isString(),
  check('age').optional(),
  validateFieldsPerson,
]

export const checkFieldDELETE = [
  check('num_doc').notEmpty().withMessage('Is required'),
  validateFieldsPerson,
]
