import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm'

@Entity('Persons')
export class ModelPerson {
  @PrimaryGeneratedColumn('uuid')
  public uuid: string

  @Column({ type: 'varchar' })
  public names: string

  @Column({ type: 'varchar' })
  public lastnames: string

  @Column({ type: 'varchar' })
  public type_doc: string

  @Column({ type: 'varchar', unique: true })
  public num_doc: string

  @Column({ type: 'numeric' })
  public age: number
}
