import { PersonEntity } from 'person/domain'
import { PersonRepository } from '../../domain/person.repository'
import { ModelPerson } from '../models/person.model'
import { AppDataSource } from '../db/TypeOrm'

export class MysqlRepository implements PersonRepository {
  private _repositoryDb = AppDataSource.getRepository(ModelPerson)

  async getAll(): Promise<any> {
    const persons = await this._repositoryDb.find()
    if (!persons) return false
    return persons.map((item) => {
      const { uuid, ...restPerson } = item
      return restPerson
    })
  }

  async getById(term: string): Promise<any> {
    const person = await this._repositoryDb.findOneBy({ num_doc: term })
    if (!person) return false
    const { uuid, ...restPerson } = person
    return restPerson
  }

  async create(person: PersonEntity): Promise<PersonEntity> {
    const personCreated = await this._repositoryDb.create(person)
    const { uuid, ...restPerson } = await this._repositoryDb.save(personCreated)
    return restPerson
  }

  async update(num_doc: string, person: PersonEntity): Promise<any> {
      const personDelted = await this.getById(num_doc)
      if (!personDelted) return false
    const personUpd = await this._repositoryDb.update({ num_doc }, person)
    return personUpd
  }

  async delete(num_doc: string): Promise<any> {
    const personDelted = await this.getById(num_doc)
    if (!personDelted) return false
    const result = await this._repositoryDb.delete({ num_doc })
    return result
  }
}
