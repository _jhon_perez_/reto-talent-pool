import { Router } from 'express'
import { MysqlRepository } from '../repository/mysql.repository'
import { PersonUseCase } from '../../application/person.useCase'
import { PersonController } from '../controller/person.controller'
import { imageUseCases } from '../../../image/infrastructure/routes/index'
import {
  checkFieldPOST,
  checkFieldDELETE,
  checkFieldGET,
  checkFieldPUT,
} from '../middleware/validateFields.middleware'

const routesPerson = Router()

// Init repository
const personRepository = new MysqlRepository()

// Init useCases
const personUseCases = new PersonUseCase(personRepository)

// Init Controller
const personController = new PersonController(personUseCases, imageUseCases)

routesPerson
  .get('/person', personController.getAllPersons)
  .get('/person/:num_doc', checkFieldGET, personController.getPersonById)
  .post('/person', checkFieldPOST, personController.create)
  .put('/person/:num_doc', checkFieldPUT, personController.updatePerson)
  .delete('/person/:num_doc', checkFieldDELETE, personController.deletePerson)

export default routesPerson
