type env = 'dev' | 'test' | 'pdn'
const NODE_ENV: env = process.env.NODE_ENV ? <env>process.env.NODE_ENV : 'dev'

const config = {
  dev: {
    server: {
      PORT: Number(process.env.PORT) || 3000,
    },
    database: {
      mongo: {
        URI_MONGO: process.env.URI_MONGO || '',
        options: {
          useUnifiedTopology: true,
        },
      },
      mysql: {
        MYSQL_HOST: process.env.MYSQL_HOST || '',
        MYSQL_PORT: Number(process.env.MYSQL_PORT) || 3306,
        MYSQL_DATABASE: process.env.MYSQL_DATABASE || '',
        MYSQL_USER: process.env.MYSQL_USER || '',
        MYSQL_PASSWORD: process.env.MYSQL_PASSWORD || '',
        MYSQL_ROOT_PASSWORD: process.env.MYSQL_ROOT_PASSWORD || '',
      },
    },
    aws: {
      s3: {
        AWS_BUCKET_NAME: process.env.AWS_BUCKET_NAME || '',
        AWS_BUCKET_REGION: process.env.AWS_BUCKET_REGION || '',
        AWS_ACCESS_PUBLIC_KEY: process.env.AWS_ACCESS_PUBLIC_KEY || '',
        AWS_ACCESS_SECRET_KEY: process.env.AWS_ACCESS_SECRET_KEY || '',
      },
    },
  },
  test: {
    server: {
      PORT: Number(process.env.PORT) || 3000,
    },
    database: {
      mongo: {
        URI_MONGO: process.env.URI_MONGO || '',
        options: {
          useUnifiedTopology: true,
        },
      },
      mysql: {
        MYSQL_HOST: process.env.MYSQL_HOST || '',
        MYSQL_PORT: Number(process.env.MYSQL_PORT) || 3306,
        MYSQL_DATABASE: process.env.MYSQL_DATABASE || '',
        MYSQL_USER: process.env.MYSQL_USER || '',
        MYSQL_PASSWORD: process.env.MYSQL_PASSWORD || '',
        MYSQL_ROOT_PASSWORD: process.env.MYSQL_ROOT_PASSWORD || '',
      },
    },
    aws: {
      s3: {
        AWS_BUCKET_NAME: process.env.AWS_BUCKET_NAME || '',
        AWS_BUCKET_REGION: process.env.AWS_BUCKET_REGION || '',
        AWS_ACCESS_PUBLIC_KEY: process.env.AWS_ACCESS_PUBLIC_KEY || '',
        AWS_ACCESS_SECRET_KEY: process.env.AWS_ACCESS_SECRET_KEY || '',
      },
    },
  },
  pdn: {
    server: {
      PORT: Number(process.env.PORT) || 8080,
    },
    database: {
      mongo: {
        URI_MONGO: process.env.URI_MONGO || '',
        options: {
          useUnifiedTopology: true,
        },
      },
      mysql: {
        MYSQL_HOST: process.env.MYSQL_HOST || '',
        MYSQL_PORT: Number(process.env.MYSQL_PORT) || 3306,
        MYSQL_DATABASE: process.env.MYSQL_DATABASE || '',
        MYSQL_USER: process.env.MYSQL_USER || '',
        MYSQL_PASSWORD: process.env.MYSQL_PASSWORD || '',
        MYSQL_ROOT_PASSWORD: process.env.MYSQL_ROOT_PASSWORD || '',
      },
    },
    aws: {
      s3: {
        AWS_BUCKET_NAME: process.env.AWS_BUCKET_NAME || '',
        AWS_BUCKET_REGION: process.env.AWS_BUCKET_REGION || '',
        AWS_ACCESS_PUBLIC_KEY: process.env.AWS_ACCESS_PUBLIC_KEY || '',
        AWS_ACCESS_SECRET_KEY: process.env.AWS_ACCESS_SECRET_KEY || '',
      },
    },
  },
}

export const configApp = { ...config[NODE_ENV] }
