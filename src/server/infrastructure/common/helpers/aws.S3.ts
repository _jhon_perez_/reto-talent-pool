import {
  S3Client,
  PutObjectCommand,
  DeleteObjectCommand,
  DeleteObjectsCommand,
} from '@aws-sdk/client-s3'
import fs from 'fs'
import { configApp } from '../configApp'

const {
  AWS_BUCKET_NAME,
  AWS_BUCKET_REGION,
  AWS_ACCESS_PUBLIC_KEY,
  AWS_ACCESS_SECRET_KEY,
} = configApp.aws.s3

const client = new S3Client({
  region: AWS_BUCKET_REGION,
  credentials: {
    accessKeyId: AWS_ACCESS_PUBLIC_KEY,
    secretAccessKey: AWS_ACCESS_SECRET_KEY,
  },
})

export async function uploadFile(file: any) {
  const dateSave = new Date().getTime()

  const newName = `${dateSave}.${file.name.split('.').at(-1)}`
  const stream = fs.createReadStream(file.tempFilePath)
  const uploadParams = {
    Bucket: AWS_BUCKET_NAME,
    Key: newName,
    Body: stream,
  }

  const comman = new PutObjectCommand(uploadParams)
  await client.send(comman)
  const url = `https://${AWS_BUCKET_NAME}.s3.${AWS_BUCKET_REGION}.amazonaws.com/${newName}`
  return { url, fileName: newName }
}

export async function deleteFiles(arrFileName: []) {
  const listKeys = arrFileName.map((item) => ({ Key: item }))
  const paramsCommand = {
    Bucket: AWS_BUCKET_NAME,
    Delete: { Objects: listKeys },
  }
  const comman = new DeleteObjectsCommand(paramsCommand)
  try {
    return await client.send(comman)
  } catch (error) {
    throw error
  }
}

export async function deleteFile(fileName: string) {
  const paramsCommand = {
    Bucket: AWS_BUCKET_NAME,
    Key: fileName,
  }
  const comman = new DeleteObjectCommand(paramsCommand)
  try {
    return await client.send(comman)
  } catch (error) {
    throw error
  }
}
