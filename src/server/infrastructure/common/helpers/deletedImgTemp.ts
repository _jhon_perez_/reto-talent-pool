const fs = require('fs').promises
const path = require('path')
const FOLDER_TO_REMOVE = './src/server/infrastructure/uploads'

export const deleteImageTem = async () => {
  fs.readdir(FOLDER_TO_REMOVE)
    .then((files: any) => {
      const unlinkPromises = files.map((file: any) => {
        const filePath = path.join(FOLDER_TO_REMOVE, file)
        return fs.unlink(filePath)
      })

      return Promise.all(unlinkPromises)
    })
    .catch((err: Error) => {
      throw err
    })
}
