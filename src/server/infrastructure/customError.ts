import { Response, Request } from 'express'

export const errorServer = (
  error: any,
  req: Request,
  res: Response,
  next: Function
) => {
  console.log(error)

  if (error?.sqlState === '23000')
    return res.status(400).json({
      statusCode: 400,
      message: 'Duplicate entry num_doc',
      data: [],
    })

  res.status(500).json({
    statusCode: 500,
    message: 'Internal error! please contact the administrator',
    data: [],
  })
}
