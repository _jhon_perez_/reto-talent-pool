import { configApp } from '../common/configApp'

const { PORT } = configApp.server
export const optionsSwagger = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'Talent Pool API',
      version: '1.0.0',
      description: 'A simple express library API',
    },
    servers: [
      {
        url: `http://localhost:${PORT}`,
      },
    ],
  },
  apis: [
    './src/person/infrastructure/routes/person.swagger.yml',
    './src/image/infrastructure/routes/image.swagger.yml',
  ],
}
