import express from 'express'
import morgan from 'morgan'
import * as http from 'http'
import fileUpload from 'express-fileupload'
import errorHandler from 'errorhandler'
import compression from 'compression'
// Swagger
import swaggerUI from 'swagger-ui-express'
import swaggerJsDoc from 'swagger-jsdoc'
//Security
import cors from 'cors'
import hpp from 'hpp'
import helmet from 'helmet'
import cookieParser from 'cookie-parser'

// custom
import routesPerson from '../../person/infrastructure/routes'
import routesImage from '../../image/infrastructure/routes'
import { errorServer } from './customError'
import { ConnectionMongo } from '../../image/infrastructure/db/MongoDb'
import { ConnectionMysql } from '../../person/infrastructure/db/TypeOrm'
import { optionsSwagger } from './doc/swaggerOption'

export class Server {
  private readonly _port: number
  private readonly _app: express.Express
  private _httpServer: http.Server
  private readonly _environment: string = process.env.NODE_ENV || 'DEV'
  private readonly _specs: any

  constructor(
    port: number,
    private readonly _connectionMongo: ConnectionMongo,
    private readonly _connectionMysql: ConnectionMysql
  ) {
    this._port = port
    this._app = express()

    this._app.use(express.json())
    this._app.use(express.urlencoded({ extended: false }))
    this._app.use(morgan('dev'))
    this._app.use(
      fileUpload({
        useTempFiles: true,
        createParentPath: true,
        tempFileDir: './src/server/infrastructure/uploads',
        preserveExtension: true,
      })
    )
    this._app.use(helmet.xssFilter())
    this._app.use(helmet.noSniff())
    this._app.use(helmet.hidePoweredBy())
    this._app.use(helmet.frameguard({ action: 'deny' }))

    this._app.use(compression())
    this._app.use(errorHandler())
    // config security
    this._app.use(cookieParser())
    this._app.use(hpp())
    this._app.use(cors())

    // Documents
    this._specs = swaggerJsDoc(optionsSwagger)
    this._app.use('/api/v1/docs', swaggerUI.serve, swaggerUI.setup(this._specs))

    // Route
    this._app.get('/', (req, res) => {
      res.status(200).json({
        statusCode: 200,
        message: 'Welcome to API  Talent Pool !!',
        data: [],
      })
    })
    this._app.use('/api/v1', routesPerson, routesImage)
    this._app.use(function (req, res, next) {
      res.status(404).json({
        statusCode: 404,
        message: 'Route API not found!',
        data: [],
      })
    })

    this._app.use(errorServer)
  }

  async start(): Promise<void> {
    try {
      await this._connectionMongo.connect()
      await this._connectionMysql.connect()
      return await new Promise((resolve) => {
        this._httpServer = this._app.listen(this._port, () => {
          console.log(
            `[${this._environment}] Server running at http://localhost:${this._port}`
          )
          resolve()
        })
      })
    } catch (error) {
      console.log(error)
      process.exit(0)
    }
  }

  async stop(): Promise<void> {
    await this._connectionMongo.close()
    await this._connectionMysql.disconnect()

    return await new Promise((resolve, reject) => {
      if (this._httpServer != null) {
        this._httpServer.close((error) => {
          if (error != null) {
            return reject(error)
          }
          return resolve()
        })
      }

      return resolve()
    })
  }

  getHTTPServer(): http.Server {
    return this._httpServer
  }
}
