import 'dotenv/config'
import request from 'supertest'
import http from 'http'
import { Server, configApp } from '../../src/server/infrastructure'
import { ConnectionMysql } from '../../src/person/infrastructure/db/TypeOrm'
import { ConnectionMongo } from '../../src/image/infrastructure/db/MongoDb'
import { deleteImageTem } from '../../src/server/infrastructure/common/helpers/deletedImgTemp'

const { PORT } = configApp.server

describe('Routes Entity Images ', () => {
  const connectionMongo = new ConnectionMongo()
  const connectionMysql = new ConnectionMysql()
  const app = new Server(PORT, connectionMongo, connectionMysql)
  let server: http.Server

  beforeAll(async () => {
    await app.stop()
    jest.setTimeout(60000)
    await app.start()
    server = app.getHTTPServer()
  })

  afterAll(async () => {
    await app.stop()
  })

  test('1. Should response method GET /api/v1/image/:doc_user', async () => {
    const result = await request(server).get('/api/v1/image/' + '12345678922')
    const resultData = {
      statusCode: 200,
      message: 'Images found successfully',
      data: [...result.body.data],
    }
    expect(result.status).toBe(200)
    expect(result.body).toMatchObject(resultData)
  })

  test('2. Should FAILED method and return status code 400 GET /api/v1/image/:doc_user ', async () => {
    const doc_user = '1kl4j1l23kj4kl12j'

    const response = await request(server).get('/api/v1/image/' + doc_user)

    const resultData = {
      statusCode: 400,
      message: `No images found by userId ${doc_user}`,
      data: false,
    }
    expect(response.status).toBe(400)
    expect(response.body).toMatchObject(resultData)
  })

  test('3. Should FAILED method POST /api/v1/image but no send userId or fileName (image)', async () => {
    const response = await request(server).post('/api/v1/image/').send({})
    const resultData = {
      statusCode: 400,
      message: 'FAILED fields!',
      data: { ...response.body.data },
    }
    expect(response.status).toBe(400)
    expect(response.body).toMatchObject(resultData)
  })

  test('4. Should response method POST /api/v1/image ', async () => {
    const response = await request(server)
      .post('/api/v1/image/')
      .attach('image', __dirname + '/Perfil.jpeg', {
        filename: 'image',
        contentType: 'application/octet-stream',
      })
      .field('doc_user', '1234567')
      .set('content-Type', 'multipart/form-data')
    const resultData = {
      statusCode: 200,
      message: 'Image saved successfully',
      data: response.body.data,
    }
    await deleteImageTem()
    expect(response.status).toBe(201)
    expect(response.body).toMatchObject(resultData)
  })
  test('6. Should FAILED method PUT  /api/v1/image/:id', async () => {
    const response = await request(server)
      .put('/api/v1/image/' + 'aaasdsa')
      .send({})
    const resultData = {
      statusCode: 400,
      message: 'FAILED fields!',
      data: { ...response.body.data },
    }
    expect(response.status).toBe(400)
    expect(response.body).toMatchObject(resultData)
  })
  test('7. Should FAILED method DELETE /api/v1/image/:doc_user', async () => {
    const doc_user = '1kl4j1l23kj4kl12j'

    const response = await request(server)
      .delete('/api/v1/image/' + doc_user)
      .send()
    const resultData = {
      statusCode: 400,
      message: `No image found by id ${doc_user}`,
      data: false,
    }
    expect(response.status).toBe(400)
    expect(response.body).toMatchObject(resultData)
  })

  test('8. Should response method DELETE /api/v1/image/:doc_user', async () => {
    const doc_user = '1234567'

    const response = await request(server)
      .delete('/api/v1/image/' + doc_user)
      .send()
    const resultData = {
      statusCode: 200,
      message: 'Image deleted successfully',
      data: true,
    }
    expect(response.status).toBe(200)
    expect(response.body).toMatchObject(resultData)
  })
})
