import 'dotenv/config'
import request from 'supertest'
import http from 'http'
import { Server, configApp } from '../../src/server/infrastructure'
import { ConnectionMysql } from '../../src/person/infrastructure/db/TypeOrm'
import { ConnectionMongo } from '../../src/image/infrastructure/db/MongoDb'
import { deleteImageTem } from '../../src/server/infrastructure/common/helpers/deletedImgTemp'

const { PORT } = configApp.server

describe('Routes Entity Person ', () => {
  const connectionMongo = new ConnectionMongo()
  const connectionMysql = new ConnectionMysql()
  const app = new Server(PORT, connectionMongo, connectionMysql)
  let server: http.Server

  beforeAll(async () => {
    await app.stop()
    jest.setTimeout(60000)
    await app.start()
    server = app.getHTTPServer()
  })

  afterAll(async () => {
    await app.stop()
  })

  test('1. Should response method GET /api/v1/person/', async () => {
    const result = await request(server).get('/api/v1/person')
    expect(result.statusCode).toBe(200)
    expect(result.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    )

    const resultData = {
      statusCode: 200,
      message: 'Persons found successfully!',
      data: [...result.body.data],
    }
    expect(result.body).toMatchObject(resultData)
  })

  test('2. Should response method GET with route /api/v1/person/num_doc', async () => {
    const { body } = await request(server).get('/api/v1/person')
    const num_doc = body.data[0].num_doc

    const result = await request(server).get('/api/v1/person/' + num_doc)
    const resultData = {
      satus: 200,
      message: 'Person found sucefully!',
      data: { ...result.body.data },
    }
    expect(result.statusCode).toBe(200)
    expect(result.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    )
    expect(result.body).toMatchObject(resultData)
  })

  test('3. Should FAILED method GET with route /api/v1/person/num_doc', async () => {
    const num_doc = 'unIdCualquiera'

    const result = await request(server).get('/api/v1/person/' + num_doc)
    expect(result.statusCode).toBe(400)
    expect(result.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    )
    const resultData = {
      statusCode: 400,
      message: 'Person no found !',
      data: [],
    }
    expect(result.body).toMatchObject(resultData)
  })

  test('4. Should FAILED method POST with route /api/v1/person by person exist', async () => {
    const { body } = await request(server).get('/api/v1/person')
    const userIn = body.data[0]

    const resultData = {
      statusCode: 400,
      message: 'Duplicate entry num_doc',
      data: [],
    }
    const result = await request(server).post('/api/v1/person').send(userIn)
    expect(result.statusCode).toBe(400)
    expect(result.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    )
    expect(result.body).toMatchObject(resultData)
  })

  let idUserCreated = ''
  test('5. Should response method POST with route /api/v1/person', async () => {
    const dataSend = {
      names: 'jhon',
      lastnames: 'Jainer',
      type_doc: 'Cedula',
      num_doc: String(Math.floor(Math.random() * 123456789)),
      age: '26',
    }
    const result = await request(server).post('/api/v1/person').send(dataSend)
    expect(result.statusCode).toBe(200)
    expect(result.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    )
    idUserCreated = result.body.data.num_doc
    const resultData = {
      statusCode: 200,
      message: 'Person saved!',
      data: {
        ...dataSend,
      },
    }
    expect(result.body).toMatchObject(resultData)
  })

  test('6. Should response method PUT with route /api/v1/person/num_doc id no exist', async () => {
    const dataSend = {
      names: 'jhon',
      lastnames: 'Jainer - Updated',
      type_doc: 'Cedula',
      age: 26,
    }
    const result = await request(server)
      .put('/api/v1/person/' + idUserCreated)
      .send(dataSend)

    expect(result.statusCode).toBe(200)
    expect(result.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    )
    const resultData = {
      statusCode: 200,
      message: 'Person Updated Successfully!',
      data: { ...dataSend },
    }
    expect(result.body).toMatchObject(resultData)
  })

  test('7. Should FAILED method PUT with route /api/v1/personnum_doc id no exist', async () => {
    const result = await request(server)
      .put('/api/v1/person/' + 'unIDFAKE')
      .send({})

    expect(result.statusCode).toBe(400)
    expect(result.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    )
    const resultData = {
      statusCode: 400,
      message: 'person no found !',
      data: [],
    }
    expect(result.body).toMatchObject(resultData)
  })

  test('8. Should response method DELETED with route /api/v1/personnum_doc', async () => {
    const result = await request(server)
      .delete('/api/v1/person/' + idUserCreated)
      .send()

    expect(result.statusCode).toBe(200)
    expect(result.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    )
    const resultData = {
      statusCode: 200,
      message: 'Person Delete Successfully!',
      data: [],
    }
    expect(result.body).toMatchObject(resultData)
  })

  test('9. Should FAILED method DELETED with route /api/v1/personnum_doc id no exist', async () => {
    const result = await request(server)
      .delete('/api/v1/person/' + 'unIDFAKE')
      .send()

    expect(result.statusCode).toBe(400)
    expect(result.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    )
    const resultData = {
      statusCode: 400,
      message: 'person no found !',
      data: [],
    }
    expect(result.body).toMatchObject(resultData)
  })
})
