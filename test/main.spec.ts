import 'dotenv/config'
import http from 'http'
import { AddressInfo } from 'node:net'
import request from 'supertest'

import { Server, configApp } from '../src/server/infrastructure'
import { ConnectionMysql } from '../src/person/infrastructure/db/TypeOrm'
import { ConnectionMongo } from '../src/image/infrastructure/db/MongoDb'

const { PORT } = configApp.server

describe('Server', () => {
  const connectionMongo = new ConnectionMongo()
  const connectionMysql = new ConnectionMysql()
  const app = new Server(PORT, connectionMongo, connectionMysql)
  let server: http.Server

  beforeAll(async () => {
    await app.start()
    server = app.getHTTPServer()
  })

  afterAll(async () => {
    await app.stop()
  })

  test('1. Should Connection db Mysql', async () => {
    await connectionMysql.disconnect()
    const result = await connectionMysql.connect()
    expect(result).toBe(true)
  })

  test('2. Should Connection db MongoDB ', async () => {
    const result = await connectionMongo.connect()
    expect(result).toBe(true)
  })
  test('3. Should start server, no is null', () => {
    expect(server).not.toBeNull()
  })

  test('4. Should start in the port, no is null ', () => {
    const address = server.address() as AddressInfo
    const { port } = address
    expect(port).toBe(Number(process.env.PORT))
  })

  test('5. Should response method GET /', async () => {
    const resService = {
      statusCode: 200,
      message: 'Welcome to API  Talent Pool !!',
      data: [],
    }
    const result = await request(server).get('/')
    expect(result.status).toBe(200)
    expect(result.text).toBe(JSON.stringify(resService))
  })
})
