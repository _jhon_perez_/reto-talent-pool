import { ImageEntity } from '../../../src/image/domain/image.Entity'
import { v4 as uuid } from 'uuid'

export const imagesData: ImageEntity[] = [
  {
    uuid: uuid(),
    doc_user: '1073326739',
    url: 'http://www.test.com/image1.png',
    fileName: 'image1.png',
  },
  {
    uuid: uuid(),
    doc_user: '1073326739',
    url: 'http://www.test.com/image2.png',
    fileName: 'image2.png',
  },
  {
    uuid: uuid(),
    doc_user: '1073326739',
    url: 'http://www.test.com/image3.png',
    fileName: 'image3.png',
  },
]
