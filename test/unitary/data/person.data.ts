import { PersonEntity } from '../../../src/person/domain/person.Entity'
import { v4 as uuid } from 'uuid'

interface PersonResponse extends PersonEntity {
  images?: string[]
}
export const personsData: PersonResponse[] = [
  {
    uuid: uuid(),
    names: 'jhon',
    lastnames: 'Lozano',
    type_doc: 'Cedula',
    num_doc: '12345678922',
    age: 26,
    images: [
      'http://www.test.com/image1.png',
      'http://www.test.com/image1.png',
      'http://www.test.com/image1.png',
      'http://www.test.com/image1.png',
      'http://www.test.com/image1.png',
    ],
  },
  {
    uuid: uuid(),
    names: 'jhon',
    lastnames: 'Lozano',
    type_doc: 'Cedula',
    num_doc: '12345678924',
    age: 26,
    images: [
      'http://www.test.com/image1.png',
      'http://www.test.com/image1.png',
      'http://www.test.com/image1.png',
      'http://www.test.com/image1.png',
      'http://www.test.com/image1.png',
    ],
  },
  {
    uuid: uuid(),
    names: 'jhon',
    lastnames: 'Lozano',
    type_doc: 'Cedula',
    num_doc: '12345678926',
    age: 26,
    images: [
      'http://www.test.com/image1.png',
      'http://www.test.com/image1.png',
      'http://www.test.com/image1.png',
      'http://www.test.com/image1.png',
      'http://www.test.com/image1.png',
    ],
  },
]
