import { ImageValue } from '../../src/image/domain/image.Value'

const uuid: Function = jest.fn(() => '0d50d083-b2b6-4a97-ad08-2f51e45c28e3')

beforeEach(() => {})

describe('Model Image', () => {
  test('1. Should creat Image', async () => {
    const newImage = new ImageValue({
      uuid: uuid(),
      doc_user: '1023456978',
      url: 'http://example.com/image.jpg',
      fileName: 'image.jpg',
    })

    expect(newImage.doc_user).toBe('1023456978')
    expect(newImage.url).toBe('http://example.com/image.jpg')
    expect(newImage.fileName).toBe('image.jpg')
  })
})
