import { imagesData } from './data/images.data'
import { MongoRepository } from '../../src/image/infrastructure/repository/mongo.Repository'
import { ImageUseCases } from '../../src/image/application/image.useCase'

jest.mock('../../src/image/infrastructure/repository/mongo.Repository')

describe('Test useCases of entity Image', () => {
  const mongoRepository = new MongoRepository()

  beforeEach(() => {
    jest.resetAllMocks()
  })

  test('1. [GetByUser] Should get the list of images', async () => {
    const arrUrlImages = imagesData.map((item) => item.url)
    const spy = jest.spyOn(mongoRepository, 'getByUser')
    spy.mockReturnValueOnce(Promise.resolve(arrUrlImages))

    const imageUseCases = new ImageUseCases(mongoRepository)
    const response = await imageUseCases.getByUser('1073326739')
    expect(response).toBe(arrUrlImages)
  })

  test('2. [GetByUser] Should failed get images, return false', async () => {
    const returnResponse = false

    const spy = jest.spyOn(mongoRepository, 'getByUser')
    spy.mockReturnValueOnce(Promise.resolve(returnResponse))

    const imageUseCases = new ImageUseCases(mongoRepository)
    const response = await imageUseCases.getByUser('idNoExiste')
    expect(response).toBe(returnResponse)
  })

  test('3. [save] Should saved the image, return info', async () => {
    const { url, fileName } = imagesData[0]

    const spy = jest.spyOn(mongoRepository, 'save')
    spy.mockReturnValueOnce(Promise.resolve({ url, fileName }))

    const imageUseCases = new ImageUseCases(mongoRepository)
    const response = await imageUseCases.save(imagesData[0])
    expect(response).toMatchObject({ url, fileName })
  })

  test('4. [update] Should update the image, return true', async () => {
    const returnResponse = true

    const spy = jest.spyOn(mongoRepository, 'update')
    spy.mockReturnValueOnce(Promise.resolve(returnResponse))

    const imageUseCases = new ImageUseCases(mongoRepository)
    const response = await imageUseCases.update('1073326739', imagesData[0])
    expect(response).toBe(returnResponse)
  })

  test('5. [update] Should failed the update image, return false', async () => {
    const returnResponse = false

    const spy = jest.spyOn(mongoRepository, 'update')
    spy.mockReturnValueOnce(Promise.resolve(returnResponse))

    const imageUseCases = new ImageUseCases(mongoRepository)
    const response = await imageUseCases.update('idFake12123', imagesData[0])
    expect(response).toBe(returnResponse)
  })

  test('6. [delete] Should delete the image, return true', async () => {
    const returnResponse = true

    const spy = jest.spyOn(mongoRepository, 'delete')
    spy.mockReturnValueOnce(Promise.resolve(returnResponse))

    const imageUseCases = new ImageUseCases(mongoRepository)
    const response = await imageUseCases.delete('1073326739')
    expect(response).toBe(returnResponse)
  })

  test('7. [delete] Should failed the delete image, return false', async () => {
    const returnResponse = false

    const spy = jest.spyOn(mongoRepository, 'delete')
    spy.mockReturnValueOnce(Promise.resolve(returnResponse))

    const imageUseCases = new ImageUseCases(mongoRepository)
    const response = await imageUseCases.delete('idFake12123')
    expect(response).toBe(returnResponse)
  })
})
