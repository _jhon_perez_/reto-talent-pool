import { PersonValue } from '../../src/person/domain/person.value'

const uuid: Function = jest.fn(() => '0d50d083-b2b6-4a97-ad08-2f51e45c28e3')

beforeEach(() => {})

describe('Model Person', () => {
  test('1. Should creat Person', async () => {
    const newImage = new PersonValue({
      uuid: uuid(),
      names: 'JhonTest',
      lastnames: 'PerezTest',
      type_doc: 'Cedula',
      num_doc: '1023445534',
      age: 25,
    })

    expect(newImage.uuid).toBe('0d50d083-b2b6-4a97-ad08-2f51e45c28e3')
    expect(newImage.names).toBe('JhonTest')
    expect(newImage.lastnames).toBe('PerezTest')
    expect(newImage.type_doc).toBe('Cedula')
    expect(newImage.num_doc).toBe('1023445534')
    expect(newImage.age).toBe(25)
  })
})
