import { personsData } from './data/person.data'
import { PersonUseCase } from '../../src/person/application/person.useCase'
import { MysqlRepository } from '../../src/person/infrastructure/repository/mysql.repository'

jest.mock('../../src/person/infrastructure/repository/mysql.repository')

describe('Test useCases of entity Person', () => {
  const mysqlRepository = new MysqlRepository()

  beforeEach(() => {
    jest.resetAllMocks()
  })

  test('1. [getPersons] Should get All persons  ', async () => {
    const returnResponse = personsData.map((item) => {
      const { uuid, ...restPerson } = item
      return restPerson
    })
    const spyRepository = jest.spyOn(mysqlRepository, 'getAll')
    spyRepository.mockReturnValueOnce(Promise.resolve(returnResponse))

    const personUseCase = new PersonUseCase(mysqlRepository)
    const response = await personUseCase.getPersons()

    expect(response).toMatchObject(returnResponse)
  })
  test('2. [getPersons] Should FAILED All persons return false  ', async () => {
    const returnResponse = false

    const spyRepository = jest.spyOn(mysqlRepository, 'getAll')
    spyRepository.mockReturnValueOnce(Promise.resolve(returnResponse))

    const personUseCase = new PersonUseCase(mysqlRepository)
    const response = await personUseCase.getPersons()

    expect(response).toBe(returnResponse)
  })

  test('3. [getPersonById] Should get person by num_doc ', async () => {
    const { uuid, ...returnResponse } = personsData[0]

    const spyRepository = jest.spyOn(mysqlRepository, 'getById')
    spyRepository.mockReturnValueOnce(Promise.resolve(returnResponse))

    const personUseCase = new PersonUseCase(mysqlRepository)
    const response = await personUseCase.getPersonById('12345678924')

    expect(response).toMatchObject(returnResponse)
  })
  test('4. [getPersonById] Should FAILED All persons return false ', async () => {
    const returnResponse = false
    const spyRepository = jest.spyOn(mysqlRepository, 'getById')
    spyRepository.mockReturnValueOnce(Promise.resolve(returnResponse))

    const personUseCase = new PersonUseCase(mysqlRepository)
    const response = await personUseCase.getPersonById('idFake12123')

    expect(response).toBe(returnResponse)
  })

  test('5. [createPerson] Should create person ', async () => {
    const { uuid, ...returnResponse } = personsData[0]

    const spyRepository = jest.spyOn(mysqlRepository, 'create')
    spyRepository.mockReturnValueOnce(Promise.resolve(returnResponse))

    const personUseCase = new PersonUseCase(mysqlRepository)
    const response = await personUseCase.createPerson(personsData[0])

    expect(response).toMatchObject(returnResponse)
  })
  test('6. [update] Should update person, return true ', async () => {
    const returnResponse = true

    const spyRepository = jest.spyOn(mysqlRepository, 'update')
    spyRepository.mockReturnValueOnce(Promise.resolve(returnResponse))

    const personUseCase = new PersonUseCase(mysqlRepository)
    const response = await personUseCase.update('12345678924', personsData[0])

    expect(response).toBe(returnResponse)
  })
  test('7. [update] Should Failed update person, return false  ', async () => {
    const returnResponse = false

    const spyRepository = jest.spyOn(mysqlRepository, 'update')
    spyRepository.mockReturnValueOnce(Promise.resolve(returnResponse))

    const personUseCase = new PersonUseCase(mysqlRepository)
    const response = await personUseCase.update('idFake12123', personsData[0])

    expect(response).toBe(returnResponse)
  })
  test('8. [delete] Should delete person, return true', async () => {
    const returnResponse = true

    const spyRepository = jest.spyOn(mysqlRepository, 'delete')
    spyRepository.mockReturnValueOnce(Promise.resolve(returnResponse))

    const personUseCase = new PersonUseCase(mysqlRepository)
    const response = await personUseCase.delete('12345678924')

    expect(response).toBe(returnResponse)
  })
  test('9. [delete] Should Failed delete person, return false ', async () => {
    const returnResponse = false

    const spyRepository = jest.spyOn(mysqlRepository, 'delete')
    spyRepository.mockReturnValueOnce(Promise.resolve(returnResponse))

    const personUseCase = new PersonUseCase(mysqlRepository)
    const response = await personUseCase.delete('idFake12123')

    expect(response).toBe(returnResponse)
  })
})
